package com.wbr.service;

import com.wbr.entity.Role;
import com.wbr.entity.User1;
import com.wbr.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements UserDetailsService {
    private final UserMapper userMapper;

    @Autowired
    public UserService(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //1.根据用户名查询用户信息
        User1 user1 = userMapper.loadUserByUsername(username);
        if (user1 == null) {
            throw new UsernameNotFoundException("用户不存在");
        }
        List<Role> roles = userMapper.getUserRoleByUid(user1.getId());
        user1.setRoles(roles);
        return user1;
    }
}