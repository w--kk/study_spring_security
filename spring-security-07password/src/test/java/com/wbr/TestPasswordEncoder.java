package com.wbr;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class TestPasswordEncoder {

    public static void main(String[] args) {

        //1.加密  代表16次内部散列，默认是10次散列
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(16);
        String encode = bCryptPasswordEncoder.encode("123");
        System.out.println(encode);

    }
}
