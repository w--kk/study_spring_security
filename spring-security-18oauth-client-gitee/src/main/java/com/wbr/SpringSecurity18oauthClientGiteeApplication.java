package com.wbr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurity18oauthClientGiteeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurity18oauthClientGiteeApplication.class, args);
    }

}
