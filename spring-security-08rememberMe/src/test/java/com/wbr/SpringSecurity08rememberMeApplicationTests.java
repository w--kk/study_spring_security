package com.wbr;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Base64Utils;

@SpringBootTest
class SpringSecurity08rememberMeApplicationTests {

    @Test
    void contextLoads() {
//        byte[] result = Base64Utils.decodeFromString("cm9vdDoxNzA3NDc5NDc3MjUxOjQ5ZTJkOWQ0NzU2OGU5MjZkZTgxODhiMzI5Z");
        byte[] result = Base64Utils.decodeFromString("dEkzaUlla3E0OHpEYVBzelRPSTFEdyUzRCUzRDp2RHRkVTVoOEVQcHdpNkxmU2VRTENBJTNEJT");
        System.out.println(new String(result));
    }

}
